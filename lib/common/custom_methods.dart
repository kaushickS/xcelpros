import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:supercharged/supercharged.dart';

showFlushBar(context, int duration, content, color) {
  return Flushbar(
    duration: duration.milliseconds,
    borderRadius: 10,
    margin: EdgeInsets.only(left: 10, right: 10, top: 10),
    flushbarPosition: FlushbarPosition.TOP,
    backgroundColor: color,
    flushbarStyle: FlushbarStyle.FLOATING,
    forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
    messageText: Align(
      alignment: Alignment.center,
      child: Text(
        content,
        style: TextStyle(
            color: Colors.white,
            fontFamily: 'QSM',
            fontSize: 18,
            letterSpacing: 2),
      ),
    ),
  )..show(context);
}
