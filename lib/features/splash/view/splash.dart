import 'package:flutter/material.dart';
import 'package:supercharged/supercharged.dart';
import 'package:xcelpros/routes/router.gr.dart';
import 'package:xcelpros/services/navigator.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Future.delayed(3.seconds, () {
      AppNavigator.pushReplacement(Routes.loginPage);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Text(
          'Xcelpros',
          style: TextStyle(
              color: Colors.blueGrey[700],
              fontFamily: 'QSB',
              fontSize: 30,
              letterSpacing: 2),
        ),
      ),
    );
  }
}
