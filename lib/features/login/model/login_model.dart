import 'package:flutter/material.dart';
import 'package:supercharged/supercharged.dart';
import 'package:xcelpros/routes/router.gr.dart';
import 'package:xcelpros/services/navigator.dart';

class LoginModel extends ChangeNotifier {
  String _flrAnimation = 'idle';
  double _containerHeight = 380;
  int _introTextIndex = 0;
  double _backOpacity = 0;
  bool _signUpFormVisibility = false;
  bool _checkBox = false;
  bool _formVisibilty = true;

  String get flrAnimation => _flrAnimation;
  double get containerHeight => _containerHeight;
  int get introTextIndex => _introTextIndex;
  double get backOpacity => _backOpacity;
  bool get signUpFormVisibility => _signUpFormVisibility;
  bool get checkBox => _checkBox;
  bool get formVisibilty => _formVisibilty;

  void setFlrAnimation(String val) {
    _flrAnimation = val;
    notifyListeners();
  }

  void setCheckBox() {
    _checkBox = _checkBox ? false : true;
    notifyListeners();
  }

  void open() {
    _containerHeight = _containerHeight == 380 ? 490 : 380;
    _introTextIndex = _introTextIndex == 0 ? 1 : 0;
    _backOpacity = _backOpacity == 0 ? 1 : 0;
    _signUpFormVisibility = _signUpFormVisibility ? false : true;
    notifyListeners();
  }

  void idleToSLogin(String anim) {
    _backOpacity = 0;
    _formVisibilty = false;
    _flrAnimation = anim;
    _containerHeight = 0;
    notifyListeners();
    Future.delayed(2.seconds, () {
      if (anim == "signup_to_slogin") {
        _flrAnimation = 'sslogin_to_login';
        _backOpacity = 0;
        _signUpFormVisibility = false;
        _introTextIndex = 0;
        _formVisibilty = true;
        _containerHeight = 380;
        notifyListeners();
      } else {
        AppNavigator.pushReplacement(Routes.dashboardPage);
      }
    });
  }

  void signUpToSLogin() {}
}
