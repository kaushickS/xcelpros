import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:validators/validators.dart';
import 'package:xcelpros/common/custom_methods.dart';
import 'package:xcelpros/features/login/model/login_model.dart';
import 'package:sized_context/sized_context.dart';
import 'package:xcelpros/res/colors.dart';
import 'package:xcelpros/services/ui.dart';

class LoginFormWidgets extends StatefulWidget {
  @override
  _LoginFormWidgetsState createState() => _LoginFormWidgetsState();
}

class _LoginFormWidgetsState extends State<LoginFormWidgets> {
  final _userController = TextEditingController();

  final _passwordController = TextEditingController();

  final _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginModel>(
      builder: (context, value, child) {
        return Visibility(
          visible: value.formVisibilty,
          child: Container(
            margin: EdgeInsets.only(top: 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                _getUserWidget(
                    _userController, 'Username', FontAwesomeIcons.user),
                _getUserWidget(
                    _passwordController, 'Password', FontAwesomeIcons.lock),
                Visibility(
                  visible: value.signUpFormVisibility,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      _getUserWidget(
                          _emailController, 'Email', FontAwesomeIcons.envelope),
                      SizedBox(
                        height: 60,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Checkbox(
                            value: value.checkBox,
                            activeColor: lightGreen,
                            onChanged: (value) =>
                                context.read<LoginModel>().setCheckBox(),
                          ),
                          RichText(
                            text: TextSpan(
                                text: 'I have accepted the   ',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontFamily: 'QSSM',
                                    fontSize: 13),
                                children: [
                                  TextSpan(
                                      text: 'Terms & Condition',
                                      style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          color: lightGreen,
                                          fontFamily: 'QSB',
                                          fontSize: 18))
                                ]),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  width: context.widthPx - 100,
                  height: 60,
                  child: RaisedButton(
                    onPressed: () {
                      value.signUpFormVisibility
                          ? _validate(context, 'signup_to_slogin')
                          : _validate(context, 'idle_to_slogin');
                    },
                    elevation: 10,
                    shape: UIService.getRoundedCorners(8.0),
                    color: lightGreen,
                    child: Text(
                      value.signUpFormVisibility ? 'SIGN UP' : 'SIGN IN',
                      style: TextStyle(
                          fontFamily: 'QSSM',
                          fontSize: 22,
                          letterSpacing: 3,
                          color: Colors.white),
                    ),
                  ),
                ),
                Visibility(
                  visible: !value.signUpFormVisibility,
                  child: RichText(
                      text: TextSpan(
                          text: "Don't have an account?  ",
                          style: TextStyle(
                              color: Colors.grey,
                              fontFamily: 'QSSM',
                              fontSize: 18,
                              letterSpacing: 1),
                          children: [
                        TextSpan(
                            text: 'SIGN UP',
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                context.read<LoginModel>().open();
                                context
                                    .read<LoginModel>()
                                    .setFlrAnimation('login_to_signup');
                              },
                            style: TextStyle(
                                color: lightGreen,
                                fontFamily: 'QSB',
                                fontSize: 22))
                      ])),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  _getUserWidget(TextEditingController editingController, hint, icon) {
    return Padding(
      padding: EdgeInsets.only(
        left: 30,
        right: 30,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          FaIcon(icon),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 35, right: 35),
              child: TextField(
                controller: editingController,
                obscureText:
                    editingController == _passwordController ? true : false,
                style: TextStyle(
                    fontFamily: 'QSSM',
                    letterSpacing: 2,
                    fontSize: 20,
                    color: Colors.blueGrey[800]),
                decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[300], width: 2),
                    ),
                    focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Colors.blueGrey[800], width: 2)),
                    hintText: hint,
                    hintStyle: TextStyle(
                        color: Colors.blueGrey,
                        fontFamily: 'QSSM',
                        fontSize: 20)),
              ),
            ),
          )
        ],
      ),
    );
  }

  _validate(context, String anim) {
    final provider = Provider.of<LoginModel>(context, listen: false);
    final userName = _userController.text;
    final password = _passwordController.text;
    final email = _emailController.text;

    if (userName.isEmpty || userName.length < 3) {
      print(userName);
      showFlushBar(context, 1000, 'Please enter valid username', darkRed);
    } else if (password.isEmpty || password.length < 6) {
      showFlushBar(context, 1000, 'Please enter valid password', darkRed);
    } else if (anim == "signup_to_slogin") {
      if (!isEmail(email)) {
        showFlushBar(context, 1000, 'Please enter valid email', darkRed);
      } else if (!provider.checkBox) {
        showFlushBar(context, 1000, 'Please accept the', darkRed);
      } else {
        provider.idleToSLogin(anim);
      }
    } else {
      provider.idleToSLogin(anim);
    }
  }
}
