import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:xcelpros/features/login/model/login_model.dart';
import 'package:xcelpros/res/colors.dart';
import 'package:supercharged/supercharged.dart';

class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<LoginModel>(
      builder: (context, value, child) {
        return Visibility(
          visible: !value.formVisibilty,
          child: Positioned(
            bottom: 80,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SpinKitFoldingCube(
                  duration: 1.seconds,
                  color: loaderGreen,
                ),
                SizedBox(
                  height: 100,
                ),
                Text(
                  "Let's get you started",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'QSSM',
                      fontSize: 20,
                      letterSpacing: 2),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
