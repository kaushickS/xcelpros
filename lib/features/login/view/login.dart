import 'dart:ui';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xcelpros/features/login/model/login_model.dart';
import 'package:supercharged/supercharged.dart';
import 'package:sized_context/sized_context.dart';
import 'package:xcelpros/features/login/widgets/form_widgets.dart';
import 'package:xcelpros/features/login/widgets/loader.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _flrAnimation = Consumer<LoginModel>(
      builder: (context, value, child) => FlareActor(
        'assets/anim/x_app_back.flr',
        alignment: Alignment.center,
        fit: BoxFit.fill,
        animation: value.flrAnimation,
      ),
    );
    return Scaffold(
        backgroundColor: Color(0XFF072029),
        body: Consumer<LoginModel>(
          builder: (context, value, child) {
            return Stack(
              alignment: Alignment.center,
              children: <Widget>[
                _flrAnimation,
                Loader(),
                Positioned(
                  top: 26,
                  left: 42,
                  child: GestureDetector(
                    onTap: () {
                      context.read<LoginModel>().open();
                      context
                          .read<LoginModel>()
                          .setFlrAnimation('signup_to_login');
                    },
                    child: AnimatedOpacity(
                      opacity: value.backOpacity,
                      duration: 500.milliseconds,
                      child: Text(
                        'Back',
                        style: TextStyle(
                            fontFamily: 'QSSM',
                            fontSize: 22,
                            letterSpacing: 2,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: value.formVisibilty,
                  child: Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 20, bottom: 25),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: IndexedStack(
                                index: value.introTextIndex,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Welcome',
                                        style: TextStyle(
                                            fontFamily: 'QSSM',
                                            letterSpacing: 2,
                                            color: Colors.white,
                                            fontSize: 25),
                                      ),
                                      SizedBox(height: 3),
                                      Text(
                                        'to Room Control',
                                        style: TextStyle(
                                            fontFamily: 'QSR',
                                            letterSpacing: 2,
                                            color: Colors.white,
                                            fontSize: 25),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    'Create New Account',
                                    style: TextStyle(
                                        fontFamily: 'QSR',
                                        letterSpacing: 2,
                                        color: Colors.white,
                                        fontSize: 25),
                                  )
                                ],
                              )),
                        ),
                        AnimatedContainer(
                            duration: 300.milliseconds,
                            width: context.widthPx,
                            height: value.containerHeight,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30),
                              ),
                            ),
                            child: LoginFormWidgets()),
                      ],
                    ),
                  ),
                )
              ],
            );
          },
        ));
  }
}
