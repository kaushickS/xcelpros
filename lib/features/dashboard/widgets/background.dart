import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sized_context/sized_context.dart';
import 'package:xcelpros/features/dashboard/model/dashboard_model.dart';

class DashBoardBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardModel>(
      builder: (context, value, child) {
        return Container(
          width: context.widthPx,
          height: context.heightPx,
          child: FlareActor(
            'assets/anim/hc_anim_dashboard.flr',
            alignment: Alignment.center,
            animation: value.animation,
            fit: BoxFit.fill,
          ),
        );
      },
    );
  }
}
