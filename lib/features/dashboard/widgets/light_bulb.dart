import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xcelpros/features/dashboard/model/dashboard_model.dart';

class LightBulb extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardModel>(
      builder: (context, value, child) {
        return Visibility(
            visible: value.topWidgetStachIndex == 0 ? false : true,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/bulb_holder.png',
                  width: 150,
                  height: 150,
                ),
                Container(
                  height: 15,
                  width: 25,
                  decoration: BoxDecoration(
                    color: value.bulbColor
                        .withOpacity(_getBlubValue(value.sliderVal)),
                    boxShadow: [
                      BoxShadow(
                          color: value.bulbColor
                              .withOpacity(_getBlubValue(value.sliderVal)),
                          blurRadius: 5.0,
                          spreadRadius: 2.0)
                    ],
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(200.0),
                        bottomRight: Radius.circular(200.0)),
                  ),
                )
              ],
            ));
      },
    );
  }

  double _getBlubValue(int sliderVal) {
    if (sliderVal == 100) {
      return 0.9;
    } else if (sliderVal.toString().length == 1) {
      return double.parse('0.0$sliderVal');
    } else {
      return double.parse('0.$sliderVal');
    }
  }
}
