import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xcelpros/features/dashboard/model/dashboard_model.dart';
import 'package:xcelpros/res/colors.dart';

class LightSelector extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardModel>(
      builder: (context, value, child) {
        return Container(
          margin: EdgeInsets.only(top: 30, left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _getLightSlider(context),
              _getTitleText('Colors'),
              _getListColors(context),
              _getTitleText('Scenes'),
              _getScenes()
            ],
          ),
        );
      },
    );
  }

  _getLightSlider(BuildContext context) {
    final provider = Provider.of<DashboardModel>(context, listen: false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _getTitleText('Intensity'),
        SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _getImage('assets/images/light_off.png'),
            Container(
              width: 300,
              child: Slider(
                value: provider.sliderVal.toDouble(),
                onChanged: (val) {
                  provider.setSliderVal(val);
                },
                min: 0,
                max: 100,
                activeColor: provider.bulbColor,
              ),
            ),
            _getImage('assets/images/light_on.png'),
          ],
        )
      ],
    );
  }

  _getImage(String val) {
    return Image.asset(val);
  }

  _getTitleText(String title) {
    return Text(
      title,
      style: TextStyle(
          color: darkBlue, fontFamily: 'QSB', fontSize: 20, letterSpacing: 1),
    );
  }

  _getListColors(BuildContext context) {
    return Expanded(
      child: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          ColorAvatar(color: Colors.red),
          ColorAvatar(color: Colors.lightGreen),
          ColorAvatar(color: Colors.blue),
          ColorAvatar(color: Colors.pink),
          ColorAvatar(color: Colors.yellow),
          Padding(
            padding: const EdgeInsets.only(left: 30),
            child: Icon(
              Icons.add,
              color: Colors.black,
            ),
          )
        ],
      ),
    );
  }

  _getScenes() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20, top: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _getButton('Birthday', 0XFFf29597, 0XFFf5af8b),
              _getButton('Party', 0XFFaf91f1, 0XFFd78df1)
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _getButton('Relax', 0XFF80ceef, 0XFF7adbee),
              _getButton('Fun', 0XFF68e18c, 0XFFa0eb88),
            ],
          )
        ],
      ),
    );
  }

  _getButton(title, c1, c2) {
    return Container(
      width: 150,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        gradient: LinearGradient(
          colors: [Color(c1), Color(c2)],
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'assets/images/light_on.png',
            color: Colors.white,
          ),
          Text(
            title,
            style: TextStyle(
                fontFamily: 'QSSM', color: Colors.white, fontSize: 16),
          )
        ],
      ),
    );
  }
}

class ColorAvatar extends StatelessWidget {
  final color;

  const ColorAvatar({Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardModel>(
      builder: (context, value, child) {
        return GestureDetector(
          onTap: () => context.read<DashboardModel>().setBulbColor(color),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              child: CircleAvatar(
                radius: 20,
                backgroundColor: color[300],
              ),
            ),
          ),
        );
      },
    );
  }
}
