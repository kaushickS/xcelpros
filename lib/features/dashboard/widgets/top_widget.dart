import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sized_context/sized_context.dart';
import 'package:xcelpros/features/dashboard/model/dashboard_model.dart';
import 'package:xcelpros/res/colors.dart';

class TopWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _profileView = Container(
      margin: EdgeInsets.only(bottom: 30),
      width: context.widthPx,
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 30),
            child: Text(
              'Control\nPanel',
              style: TextStyle(
                  fontFamily: 'QSR',
                  fontWeight: FontWeight.w800,
                  letterSpacing: 2,
                  color: Colors.white,
                  fontSize: 30),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Image.asset('assets/images/user.png'),
          )
        ],
      ),
    );

    final _lightViewWidget = Container(
        width: context.widthPx,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () => context.read<DashboardModel>().showControlPanel(),
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: RichText(
                  text: TextSpan(
                      text: '← Bed\nRooms',
                      style: TextStyle(
                          color: Colors.white,
                          letterSpacing: 2,
                          fontFamily: 'QSB',
                          fontSize: 30),
                      children: [
                        TextSpan(
                            text: '\n4 Lights',
                            style: TextStyle(
                                color: lightTextColor,
                                fontFamily: 'QSB',
                                fontSize: 20))
                      ]),
                ),
              ),
            ),
            Container(
                width: context.widthPx,
                height: 100,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 30,
                      ),
                      _getLightButtons('assets/images/main_lights.png',
                          Colors.white, 'Main Light'),
                      _getLightButtons('assets/images/furniture.png',
                          lightGreen, 'Desk Lights'),
                      _getLightButtons(
                          'assets/images/bed_two.png', Colors.white, 'Bed'),
                    ],
                  ),
                ))
          ],
        ));

    return Consumer<DashboardModel>(
      builder: (context, value, child) {
        return IndexedStack(
          index: value.topWidgetStachIndex,
          children: <Widget>[
            _profileView,
            Visibility(
              visible: value.topWidgetStachIndex == 0 ? false : true,
              child: _lightViewWidget,
            )
          ],
        );
      },
    );
  }

  _getLightButtons(image, color, title) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 120,
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: color,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(image),
            Text(
              title,
              style: TextStyle(
                  fontFamily: 'QSSM',
                  color: color == lightGreen ? Colors.white : lightGreen),
            )
          ],
        ),
      ),
    );
  }
}
