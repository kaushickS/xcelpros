import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xcelpros/features/dashboard/model/dashboard_model.dart';
import 'package:xcelpros/res/colors.dart';
import 'package:xcelpros/services/model/control_panel.model.dart';
import 'package:xcelpros/services/ui.dart';

class ControlPannel extends StatelessWidget {
  final List<ControlPanelModel> _model = [
    ControlPanelModel('assets/images/bed.png', 'Bed Room', '4'),
    ControlPanelModel('assets/images/room.png', 'Living Room', '1'),
    ControlPanelModel('assets/images/kitchen.png', 'Kitchen', '5'),
    ControlPanelModel('assets/images/bathtube.png', 'Bathroom', '1'),
    ControlPanelModel('assets/images/house.png', 'Outdoor', '5'),
    ControlPanelModel('assets/images/balcony.png', 'Balcony', '2'),
  ];

  @override
  Widget build(BuildContext context) {
    return Consumer<DashboardModel>(
      builder: (context, value, child) {
        return Container(
          margin: EdgeInsets.only(left: 0, top: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 20),
                child: Text(
                  'All Rooms',
                  style: TextStyle(
                      color: darkBlue,
                      fontFamily: 'QSB',
                      fontSize: 25,
                      letterSpacing: 1),
                ),
              ),
              Expanded(
                child: GridView.builder(
                  itemCount: _model.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 1.2),
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(
                          left: 15, right: 15, top: 3, bottom: 3),
                      child: GestureDetector(
                        onTap: () =>
                            context.read<DashboardModel>().showLights(),
                        child: Card(
                          elevation: 2,
                          color: Colors.white,
                          shape: UIService.getRoundedCorners(40),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 10,
                                ),
                                Image.asset(_model[index].image),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  _model[index].title,
                                  style: TextStyle(
                                      fontFamily: 'QSSM',
                                      fontSize: 20,
                                      letterSpacing: 1),
                                ),
                                Text(
                                  "${_model[index].lightCount} Lights",
                                  style: TextStyle(
                                      color: lightTextColor,
                                      fontFamily: 'QSB',
                                      fontSize: 17,
                                      letterSpacing: 1),
                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
