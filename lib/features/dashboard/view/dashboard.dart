import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:xcelpros/features/dashboard/model/dashboard_model.dart';
import 'package:xcelpros/features/dashboard/widgets/background.dart';
import 'package:supercharged/supercharged.dart';
import 'package:sized_context/sized_context.dart';
import 'package:xcelpros/features/dashboard/widgets/control_pannel.dart';
import 'package:xcelpros/features/dashboard/widgets/light_bulb.dart';
import 'package:xcelpros/features/dashboard/widgets/light_selector.dart';
import 'package:xcelpros/features/dashboard/widgets/top_widget.dart';
import 'package:xcelpros/res/colors.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var mainBack = Consumer<DashboardModel>(
      builder: (context, value, child) {
        return Stack(
          alignment: Alignment.center,
          children: <Widget>[
            DashBoardBackground(),
            Positioned(
              top: 0,
              right: 30,
              child: LightBulb(),
            ),
            Positioned(
              bottom: 0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TopWidget(),
                  AnimatedContainer(
                    duration: 300.milliseconds,
                    decoration: BoxDecoration(
                      color: countrolPanelBackColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    width: context.widthPx,
                    height: value.containerHeight,
                    child: IndexedStack(
                      index: value.topWidgetStachIndex,
                      children: <Widget>[ControlPannel(), LightSelector()],
                    ),
                  ),
                ],
              ),
            )
          ],
        );
      },
    );
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(child: Consumer<DashboardModel>(
              builder: (context, value, child) {
                return IndexedStack(
                  index: value.mainStackIndex,
                  children: <Widget>[
                    Miscellaneous(
                      title: 'This is Light page',
                    ),
                    mainBack,
                    Miscellaneous(
                      title: 'This is Settings  page',
                    ),
                  ],
                );
              },
            )),
            Container(
              width: context.widthPx,
              height: 50,
              color: Colors.white,
              child: Consumer<DashboardModel>(
                builder: (context, value, child) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () =>
                            context.read<DashboardModel>().setMainStackIndex(0),
                        child: Image.asset('assets/images/bulb.png'),
                      ),
                      GestureDetector(
                        onTap: () =>
                            context.read<DashboardModel>().setMainStackIndex(1),
                        child: Image.asset('assets/images/home.png'),
                      ),
                      GestureDetector(
                        onTap: () =>
                            context.read<DashboardModel>().setMainStackIndex(2),
                        child: Image.asset('assets/images/setting.png'),
                      ),
                    ],
                  );
                },
              ),
            )
          ],
        ));
  }
}

class Miscellaneous extends StatelessWidget {
  final title;

  const Miscellaneous({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        DashBoardBackground(),
        Text(
          title,
          style:
              TextStyle(fontFamily: 'QSSM', color: Colors.white, fontSize: 20),
        )
      ],
    );
  }
}
