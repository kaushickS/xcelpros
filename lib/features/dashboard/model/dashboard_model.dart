import 'package:flutter/material.dart';

class DashboardModel extends ChangeNotifier {
  String _animation = 'idle';
  double _containerHeight = 530;
  int _topWidgetStachIndex = 0;
  int _sliderVal = 50;
  Color _bulbColor = Colors.yellow;
  int _mainStackIndex = 1;

  String get animation => _animation;
  double get containerHeight => _containerHeight;
  int get topWidgetStachIndex => _topWidgetStachIndex;
  int get sliderVal => _sliderVal;
  Color get bulbColor => _bulbColor;
  int get mainStackIndex => _mainStackIndex;

  setAnim(String anim) {
    _animation = anim;
    notifyListeners();
  }

  setMainStackIndex(int val) {
    _mainStackIndex = val;
    notifyListeners();
  }

  setSliderVal(double val) {
    _sliderVal = val.toInt();
    notifyListeners();
  }

  setBulbColor(Color color) {
    _bulbColor = color;
    notifyListeners();
  }

  showLights() {
    _topWidgetStachIndex = _topWidgetStachIndex == 0 ? 1 : 0;
    _containerHeight = 420;
    _animation = 'forward';
    notifyListeners();
  }

  showControlPanel() {
    _topWidgetStachIndex = _topWidgetStachIndex == 0 ? 1 : 0;
    _containerHeight = 530;
    _animation = 'reverse';
    notifyListeners();
  }
}
