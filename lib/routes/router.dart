import 'package:auto_route/auto_route_annotations.dart';
import 'package:xcelpros/features/dashboard/view/dashboard.dart';
import 'package:xcelpros/features/login/view/login.dart';
import 'package:xcelpros/features/splash/view/splash.dart';

@MaterialAutoRouter(routes: [
  MaterialRoute(page: SplashPage, initial: true),
  AdaptiveRoute(page: LoginPage),
  AdaptiveRoute(page: DashboardPage)
])
class $Router {}
