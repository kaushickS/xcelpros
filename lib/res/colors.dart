import 'package:flutter/material.dart';

const lightGreen = Color(0XFF47a79b);
const loaderGreen = Color(0XFF62dcbb);
final darkRed = Colors.red[800];
final darkBlue = Color(0XFF022d67);
final countrolPanelBackColor = Color(0XFFf6f8fb);
final lightTextColor = Color(0XFFf5a841);
