import 'package:auto_route/auto_route.dart';

class AppNavigator {
  static pushName(className) {
    ExtendedNavigator.root.pushNamed(className);
  }

  static pop() {
    ExtendedNavigator.root.pop();
  }

  static pushReplacement(className) {
    ExtendedNavigator.root.pushReplacementNamed(className);
  }
}
