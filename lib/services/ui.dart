import 'package:flutter/material.dart';

class UIService {
  static getRoundedCorners(double val) {
    return RoundedRectangleBorder(borderRadius: BorderRadius.circular(val));
  }
}
