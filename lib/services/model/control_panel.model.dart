class ControlPanelModel {
  final image, title, lightCount;

  ControlPanelModel(this.image, this.title, this.lightCount);
}
